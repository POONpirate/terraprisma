local L = locale ~= "zh" and locale ~= "zhr"

name = L and "Terraprisma" or "泰拉光棱剑"
description = L and "To some extent, it restored the terraprisma.\nIf you think it's interesting,please give me a like." 
or "在一定程度上还原了泰拉瑞亚光棱剑。\n如果你觉得这个模组还不错，请为我点个赞。"
author = "WIGFRID"
version = "1.1.2"

forumthread = ""

dst_compatible = true
dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true
hamlet_compatible = true
all_clients_require_mod= true
api_version = 6
api_version_dst = 10
server_filter_tags = {"Terraprisma"}

icon_atlas = "modicon.xml"
icon = "modicon.tex"

configuration_options =
{
	L and
	{
        name="language",
        label="language",
		hover = "language",
        options={
            {description="Chinese",data=true},
            {description="English",data=false},
        },
        default=false
    } or
    {
        name="language",
        label="语言",
		hover = "语言",
        options={
            {description="简体中文",data=true},
            {description="English",data=false},
        },
        default=true
    },

    L and
	{
        name="durability",
        label="durability",
		hover = "durability",
        options={
            {description="50",data=50},
            {description="100",data=100},
            {description="150",data=150},
            {description="200",data=200},
            {description="300",data=300},
            {description="500",data=500},
            {description="800",data=800},
            {description="Infinity",data=-1},
        },
        default=150
    } or
    {
        name="durability",
        label="耐久度",
		hover = "耐久度",
        options={
            {description="50",data=50},
            {description="100",data=100},
            {description="150",data=150},
            {description="200",data=200},
            {description="300",data=300},
            {description="500",data=500},
            {description="800",data=800},
            {description="无限",data=-1},
        },
        default=150
    },

    L and
	{
        name="damage",
        label="damage",
		hover = "damage",
        options={
            {description="1",data=1},
            {description="10",data=10},
            {description="20",data=20},
            {description="34",data=34},
            {description="50",data=50},
            {description="68",data=68},
            {description="80",data=80},
            {description="108",data=108},
            {description="168",data=168}
        },
        default=50
    } or
    {
        name="damage",
        label="伤害",
		hover = "伤害",
        options={
            {description="1",data=1},
            {description="10",data=10},
            {description="20",data=20},
            {description="34",data=34},
            {description="50",data=50},
            {description="68",data=68},
            {description="80",data=80},
            {description="108",data=108},
            {description="168",data=168}
        },
        default=50
    },

    L and
	{
        name="make",
        label="make/from the Champion",
		hover = "make/from the Champion",
        options={
            {description="make",data=true},
            {description="drop",data=false}
        },
        default=true
    } or
    {
        name="make",
        label="制作/天体英雄掉落",
		hover = "制作/天体英雄掉落",
        options={
            {description="制作",data=true},
            {description="掉落",data=false}
        },
        default=true
    },

    L and
	{
        name="recipe",
        label="recipe difficulty",
		hover = "recipe difficulty（if can make）",
        options={
            {description="easy",data=0},
            {description="hard",data=1}
        },
        default=0
    } or
    {
        name="recipe",
        label="制作难度",
		hover = "制作难度(如果能做)",
        options={
            {description="简单",data=0},
            {description="困难",data=1}
        },
        default=0
    },

    L and
	{
        name="slot",
        label="equip slot",
		hover = "equip slot",
        options={
            {description="hand",data=0},
            {description="body",data=1},
            {description="neck",data=2}
        },
        default=0
    } or
    {
        name="slot",
        label="装备栏位",
		hover = "装备栏位",
        options={
            {description="武器",data=0},
            {description="防具",data=1},
            {description="护符",data=2}
        },
        default=0
    },

    L and
	{
        name="number",
        label="number of swords",
		hover = "number of swords",
        options={
            {description="1",data=1},
            {description="2",data=2},
            {description="3",data=3},
            {description="4",data=4},
            {description="5",data=5},
            {description="6",data=6}
        },
        default=3
    } or
    {
        name="number",
        label="魔剑数量",
		hover = "魔剑数量",
        options={
            {description="1",data=1},
            {description="2",data=2},
            {description="3",data=3},
            {description="4",data=4},
            {description="5",data=5},
            {description="6",data=6}
        },
        default=3
    },

    L and
	{
        name="shining",
        label="whether bloom",
		hover = "whether bloom",
        options={
            {description="yes",data=true},
            {description="no",data=false}
        },
        default=false
    } or
    {
        name="shining",
        label="泛光效果",
		hover = "泛光效果（高光）",
        options={
            {description="是",data=true},
            {description="否",data=false}
        },
        default=false
    },

    L and
	{
        name="display",
        label="display effect",
		hover = "display effect",
        options={
            {description="rainbow",data=1},
            {description="transparent",data=2},
            {description="normal",data=3}
        },
        default=1
    } or
    {
        name="display",
        label="剑的显示效果",
		hover = "剑的显示效果",
        options={
            {description="彩虹",data=1},
            {description="透明",data=2},
            {description="普通",data=3}
        },
        default=1
    },

    L and
	{
        name="circle",
        label="surround player",
		hover = "whether surround player",
        options={
            {description="yes",data=true},
            {description="no",data=false}
        },
        default=false
    } or
    {
        name="circle",
        label="剑环绕玩家",
		hover = "剑环绕玩家",
        options={
            {description="是",data=true},
            {description="否",data=false}
        },
        default=false
    },

    L and
	{
        name="radius",
        label="surround radius",
		hover = "surround radius",
        options={
            {description="1.75",data=1.75},
            {description="2",data=2},
            {description="2.25",data=2.25},
            {description="2.5",data=2.5},
            {description="2.75",data=2.75},
            {description="3",data=3},
            {description="3.25",data=3.25},
            {description="3.5",data=3.5},
            {description="3.75",data=3.75}
        },
        default=2.5
    } or
    {
        name="radius",
        label="环绕半径",
		hover = "环绕半径",
        options={
            {description="1.75",data=1.75},
            {description="2",data=2},
            {description="2.25",data=2.25},
            {description="2.5",data=2.5},
            {description="2.75",data=2.75},
            {description="3",data=3},
            {description="3.25",data=3.25},
            {description="3.5",data=3.5},
            {description="3.75",data=3.75}
        },
        default=2.5
    },

    L and
	{
        name="auto",
        label="auto attack",
		hover = "auto attack",
        options={
            {description="yes",data=true},
            {description="no",data=false}
        },
        default=true
    } or
    {
        name="auto",
        label="自动攻击",
		hover = "自动攻击",
        options={
            {description="是",data=true},
            {description="否",data=false}
        },
        default=true
    },

    L and
	{
        name="repairable",
        label="repairable",
		hover = "repairable",
        options={
            {description="yes",data=true},
            {description="no",data=false}
        },
        default=true
    } or
    {
        name="repairable",
        label="是否可修复",
		hover = "是否可修复",
        options={
            {description="是",data=true},
            {description="否",data=false}
        },
        default=true
    },

    L and
	{
        name="fix",
        label="repair coefficient",
		hover = "repair coefficient",
        options={
            {description="0.5",data=0.5},
            {description="0.75",data=0.75},
            {description="1",data=1},
            {description="1.5",data=1.5},
            {description="2",data=2}
        },
        default=1
    } or
    {
        name="fix",
        label="修复系数",
		hover = "修复系数",
        options={
            {description="0.5",data=0.5},
            {description="0.75",data=0.75},
            {description="1",data=1},
            {description="1.5",data=1.5},
            {description="2",data=2}
        },
        default=1
    },
}