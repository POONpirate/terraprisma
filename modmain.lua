GLOBAL.setmetatable(env,{__index=function(t,k) return GLOBAL.rawget(GLOBAL,k) end})

Assets = {
    Asset("ATLAS", "images/inventoryimages/terraprisma.xml"),
    Asset("IMAGE", "images/inventoryimages/terraprisma.tex")    
}

PrefabFiles =  {
    "terraprisma",
    "terraprisma_magic"
}

TUNING.TERRAPRISMA_DURABILITY=GetModConfigData("durability")
TUNING.TERRAPRISMA_DAMAGE=GetModConfigData("damage")
TUNING.TERRAPRISMA_LANGUAGE=GetModConfigData("language")
TUNING.TERRAPRISMA_MAKE=GetModConfigData("make")
TUNING.TERRAPRISMA_RECIPE=GetModConfigData("recipe")
TUNING.TERRAPRISMA_SLOT=GetModConfigData("slot")
TUNING.TERRAPRISMA_NUMBER=GetModConfigData("number")
TUNING.TERRAPRISMA_SHINING=GetModConfigData("shining")
TUNING.TERRAPRISMA_DISPLAY=GetModConfigData("display")
TUNING.TERRAPRISMA_CIRCLE=GetModConfigData("circle")
TUNING.TERRAPRISMA_RADIUS=GetModConfigData("radius")
TUNING.TERRAPRISMA_AUTO=GetModConfigData("auto")
TUNING.TERRAPRISMA_REPAIRABLE=GetModConfigData("repairable")
TUNING.TERRAPRISMA_FIX=GetModConfigData("fix")

AddPrefabPostInit("terraprisma",function (inst)
    if TUNING.TERRAPRISMA_DURABILITY==-1 and inst.components.finiteuses then
        inst:RemoveComponent("finiteuses")
    end
end)

local language = TUNING.TERRAPRISMA_LANGUAGE
if language then
    STRINGS.NAMES.TERRAPRISMA="泰拉光棱剑"
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.TERRAPRISMA="召唤一把魔法剑为你而战。"
    STRINGS.RECIPE_DESC.TERRAPRISMA = "召唤一把魔法剑为你而战。"
else
    STRINGS.NAMES.TERRAPRISMA="Terraprisma"
    STRINGS.CHARACTERS.GENERIC.DESCRIBE.TERRAPRISMA="Summon a magic sword to fight for you."
    STRINGS.RECIPE_DESC.TERRAPRISMA = "Summon a magic sword."
end

if TUNING.TERRAPRISMA_MAKE then
    if TUNING.TERRAPRISMA_RECIPE==0 then
        local terra_prisma = AddRecipe(
            "terraprisma",
            {Ingredient("glasscutter", 1),Ingredient("bluegem", 1),Ingredient("redgem", 1),Ingredient("greengem", 1)},
            RECIPETABS.MAGIC,
            TECH.MAGIC_TWO,
            nil,nil, nil, 1, nil,
            "images/inventoryimages/terraprisma.xml",
            "terraprisma.tex"
        )
        terra_prisma.sortkey = 0
    elseif TUNING.TERRAPRISMA_RECIPE==1 then
        local terra_prisma = AddRecipe(
            "terraprisma",
            {Ingredient("glasscutter", 3),Ingredient("alterguardianhatshard", 2),Ingredient("redmooneye", 2),Ingredient("greenmooneye", 2)},
            RECIPETABS.MAGIC,
            TECH.MAGIC_TWO,
            nil,nil, nil, 1, nil,
            "images/inventoryimages/terraprisma.xml",
            "terraprisma.tex"
        )
        terra_prisma.sortkey = 0
    end

else
    AddPrefabPostInit("alterguardian_phase3",function (inst)
        if TheNet:GetIsServer() then
            if inst.components.lootdropper then
                inst.components.lootdropper:AddChanceLoot("terraprisma",1)
            end
        end
    end)
end

if TUNING.TERRAPRISMA_SLOT~=0 then
    modimport("main/enemyselect.lua")
end

if TUNING.TERRAPRISMA_DURABILITY~=-1 and TUNING.TERRAPRISMA_REPAIRABLE then
    modimport("main/prismafix.lua")
end