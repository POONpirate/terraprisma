local radian = math.pi/180
local R=TUNING.TERRAPRISMA_RADIUS
local EnemySelect = Class(function (self,inst)
    self.inst=inst
    if TUNING.TERRAPRISMA_CIRCLE then
        self.num=TUNING.TERRAPRISMA_NUMBER
        self.circle=0
        self.circle_angle=self.circle*radian
        self.per_angle=2*math.pi/self.num
        self.inst:StartUpdatingComponent(self)
        self.positions={
            [1]={},
            [2]={},
            [3]={},
            [4]={},
            [5]={},
            [6]={}
        }
        local x,_,z=self.inst.Transform:GetWorldPosition()
        for i = 0, self.num-1 do
            self.positions[i+1].x=x+R*math.sin(self.circle_angle+self.per_angle*i)
            self.positions[i+1].z=z+R*math.cos(self.circle_angle+self.per_angle*i)
        end
    end
end)

function EnemySelect:OnUpdate(dt)
    local x,_,z=self.inst.Transform:GetWorldPosition()
    self.circle=self.circle+dt*120
    if self.circle>180 then
        self.circle=self.circle-360
    end
    self.circle_angle=self.circle*radian
    for i = 0, self.num-1 do
        self.positions[i+1].x=x+R*math.sin(self.circle_angle+self.per_angle*i)
        self.positions[i+1].z=z+R*math.cos(self.circle_angle+self.per_angle*i)
    end
end
return EnemySelect