local PrismaFix = Class(function (self,inst)
    self.inst=inst
    self.fix=100
end)

function PrismaFix:SetFix(val)
    self.fix=val
end

function PrismaFix:OnSave()
    return{fix=self.fix}
end

function PrismaFix:OnLoad(data)
    self.fix=data and data.fix or 100
end

return PrismaFix