local assets =
{
    Asset("ANIM", "anim/terraprisma_blue.zip"),
    Asset("ANIM", "anim/terraprisma_red.zip"),
    Asset("ANIM", "anim/terraprisma_green.zip"),
    Asset("ANIM", "anim/terraprisma_yellow.zip"),
    Asset("ANIM", "anim/terraprisma_purple.zip"),
    Asset("ANIM", "anim/terraprisma_orange.zip"),
    Asset("SHADER", "shaders/rainbow.ksh"),
    Asset("SHADER", "shaders/red.ksh"),
    Asset("SHADER", "shaders/green.ksh"),
    Asset("SHADER", "shaders/blue.ksh"),
    Asset("SHADER", "shaders/yellow.ksh"),
    Asset("SHADER", "shaders/orange.ksh"),
    Asset("SHADER", "shaders/purple.ksh"),
}

local colours =
{
    "red",
    "green",
    "blue",
    "yellow",
    "orange",
    "purple"
}

local colours_={
    {200/255, 100/255, 100/255},
    {100/255, 200/255, 100/255},
    {100/255, 100/255, 200/255},
    {200/255, 200/255, 0/255},
    {255/255, 145/255, 0/255},
    {200/255, 0/255, 200/255}
}


local function makesword(colour,colour_)

    local function fn()
        local inst = CreateEntity()
    
        inst.entity:AddTransform()
        inst.entity:AddAnimState()
        inst.entity:AddNetwork()
        inst.entity:AddLight()
        inst.entity:SetCanSleep(false)
    
        MakeInventoryPhysics(inst)
    
        inst.Light:SetFalloff(0.5)
        inst.Light:SetIntensity(0.8)
        inst.Light:SetRadius(1.2)
        inst.Light:SetColour(colour_[1],colour_[2],colour_[3])
        inst.Light:Enable(true)
        inst.Light:EnableClientModulation(true)
    
        inst.Physics:ClearCollidesWith(COLLISION.LIMITS)
        inst.Physics:ClearCollisionMask()
        inst.Physics:CollidesWith(COLLISION.GROUND)
        
        if TUNING.TERRAPRISMA_SHINING then
            inst.AnimState:SetBloomEffectHandle("shaders/anim.ksh")
        end
        if TUNING.TERRAPRISMA_DISPLAY==1 then
            inst.AnimState:SetDefaultEffectHandle(resolvefilepath("shaders/rainbow.ksh"))
        elseif TUNING.TERRAPRISMA_DISPLAY==2 then
            inst.AnimState:SetDefaultEffectHandle(resolvefilepath("shaders/"..colour..".ksh"))
        end

        inst.AnimState:SetBank("terraprisma_"..colour)
        inst.AnimState:SetBuild("terraprisma_"..colour)
        inst.AnimState:PlayAnimation("idle")
    
        inst:AddTag("NOCLICK")
        inst:AddTag("NOBLOCK")
    
        MakeInventoryFloatable(inst, "med", 0.05, {1.1, 0.5, 1.1}, true, -9)
    
        inst.entity:SetPristine()
    
        if not TheWorld.ismastersim then
            return inst
        end
        inst.persists = false
    
        inst:AddComponent("summon_controller")
    
        inst:ListenForEvent("onshoot", function (inst,data)
            inst.AnimState:SetOrientation( ANIM_ORIENTATION.OnGround)
        end)
    
        MakeHauntableLaunch(inst)
    
        return inst
    end

    return Prefab("terraprisma_"..colour,fn,assets)
end

return makesword(colours[1],colours_[1]),
       makesword(colours[2],colours_[2]),
       makesword(colours[3],colours_[3]),
       makesword(colours[4],colours_[4]),
       makesword(colours[5],colours_[5]),
       makesword(colours[6],colours_[6])