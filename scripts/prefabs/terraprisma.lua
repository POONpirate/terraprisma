local assets =
{
    Asset("ATLAS","images/inventoryimages/terraprisma.xml"),
    Asset("ANIM", "anim/terraprisma.zip")
}

local colours =
{
    "red","green","blue",
    "yellow","orange","purple"
}

local function onequip(inst, owner)
    inst.summons={}
    for i = 1, TUNING.TERRAPRISMA_NUMBER, 1 do
        inst.summons[i]=SpawnPrefab("terraprisma_"..colours[i])
        inst.summons[i].components.summon_controller:Init(owner,-0.5+0.25*i,inst,i)
    end
end

local function onunequip(inst, owner)
    for index, value in ipairs(inst.summons) do
        if value and value:IsValid() then
            value:Remove()
        end
    end
end

local function shoot(inst,attacker,target)
    for index, value in ipairs(inst.summons) do
        if value and value:IsValid() then
            value.components.summon_controller:Shoot(target)
        end
    end
end

local function fn()
    local inst = CreateEntity()

    inst.entity:AddTransform()
    inst.entity:AddAnimState()
    inst.entity:AddNetwork()
    inst.entity:AddSoundEmitter()

    MakeInventoryPhysics(inst)

    inst.AnimState:SetBank("terraprisma")
    inst.AnimState:SetBuild("terraprisma")
    inst.AnimState:PlayAnimation("idle")

    inst:AddTag("weapon")
    inst:AddTag("terraprisma")

    MakeInventoryFloatable(inst, "med", 0.05, {1.1, 0.5, 1.1}, true, -9)

    inst:AddComponent("enemyselect")

    inst.entity:SetPristine()

    if not TheWorld.ismastersim then
        return inst
    end

    inst:AddComponent("weapon")
	inst.components.weapon:SetDamage(0)
    inst.components.weapon:SetRange(16, 16)
    inst.components.weapon:SetOnAttack(shoot)


    inst:AddComponent("finiteuses")
    inst.components.finiteuses:SetMaxUses(TUNING.TERRAPRISMA_DURABILITY)
    inst.components.finiteuses:SetUses(TUNING.TERRAPRISMA_DURABILITY)
    inst.components.finiteuses:SetConsumption("ATTACK",0)
    inst.components.finiteuses:SetOnFinished(function ()
        for index, value in ipairs(inst.summons) do
            if value and value:IsValid() then
                value:Remove()
            end
        end
        inst:Remove()
    end)
    inst:AddComponent("inspectable")

    inst:AddComponent("inventoryitem")
    inst.components.inventoryitem.imagename = "terraprisma"
    inst.components.inventoryitem.atlasname = "images/inventoryimages/terraprisma.xml"

    inst:AddComponent("equippable")
    if TUNING.TERRAPRISMA_SLOT==0 then
        inst.components.equippable.equipslot = EQUIPSLOTS.HANDS
    elseif TUNING.TERRAPRISMA_SLOT==1 then
        inst.components.equippable.equipslot = EQUIPSLOTS.BODY
    elseif TUNING.TERRAPRISMA_SLOT==2 then
        inst.components.equippable.equipslot = EQUIPSLOTS.NECK or EQUIPSLOTS.BODY
    end
    inst.components.equippable:SetOnEquip(onequip)
    inst.components.equippable:SetOnUnequip(onunequip)

    MakeHauntableLaunch(inst)

    return inst
end

return Prefab("terraprisma", fn, assets)