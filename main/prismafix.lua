GLOBAL.setmetatable(env,{__index=function(a,b)return GLOBAL.rawget(GLOBAL,b)end})

local materials={
    {"bluegem",30},
    {"redgem",35},
    {"purplegem",50},
    {"orangegem",100},
    {"yellowgem",150},
    {"greengem",250},
    {"opalpreciousgem",300},
    {"alterguardianhatshard",500}
}

for index, value in ipairs(materials) do
    AddPrefabPostInit(value[1],function (inst)
        inst:AddComponent("prismafix")
        inst.components.prismafix:SetFix(value[2])
    end)
end


local ACTIONS = GLOBAL.ACTIONS
local ActionHandler = GLOBAL.ActionHandler

local id = "PRISMAFIX"
local name = "充能"
local language = TUNING.TERRAPRISMA_LANGUAGE
if not language then
    name = "Charge"
end

local fn = function(act)
    act.target.SoundEmitter:PlaySound("turnoftides/common/together/moon_glass/mine")
    local total=act.target.components.finiteuses.total
    local current=act.target.components.finiteuses.current
    local fix=TUNING.TERRAPRISMA_FIX*act.invobject.components.prismafix.fix
    act.target.components.finiteuses:SetUses(
        current+fix<total and
        current+fix or
        total
    )
    local num=act.invobject.components.stackable and
    act.invobject.components.stackable:StackSize() or 1
    if num>1 then
        act.invobject.components.stackable:SetStackSize(num-1)
    else
        act.invobject:Remove()
    end
    return true
end

AddAction(id,name,fn)

local type = "USEITEM"
local component = "prismafix"
local testfn = function(inst, doer, target, actions)
    if target:HasTag("terraprisma")  then
        table.insert(actions, ACTIONS.PRISMAFIX)  
    end
end

AddComponentAction(type, component, testfn)

local state = "doshortaction"
AddStategraphActionHandler("wilson",ActionHandler(ACTIONS.PRISMAFIX, state))
AddStategraphActionHandler("wilson_client",ActionHandler(ACTIONS.PRISMAFIX,state))