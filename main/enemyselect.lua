GLOBAL.setmetatable(env,{__index=function(a,b)return GLOBAL.rawget(GLOBAL,b)end})

local ACTIONS = GLOBAL.ACTIONS
local ActionHandler = GLOBAL.ActionHandler

local id = "ENEMYSELECT"
local name = "选择"
local language = TUNING.TERRAPRISMA_LANGUAGE
if not language then
    name = "Select"
end

local fn = function(act)
    local inst=nil
    if act.doer.components.inventory then
        for key, value in pairs(act.doer.components.inventory.equipslots) do
            if value.components.enemyselect then
                inst=value
            end
        end
        if inst~=nil and inst.summons then
            for index, value in ipairs(inst.summons) do
                if value and value:IsValid() and act.target then
                    value.components.summon_controller:Shoot(act.target)
                end
            end
            return true
        end
    end
end

AddAction(id,name,fn)
ACTIONS.ENEMYSELECT.distance=16

local type = "SCENE"
local component = "health"
local testfn = function(inst, doer, actions, right)
    if right and doer.replica.combat ~= nil
    and doer.replica.inventory:EquipHasTag("terraprisma")
    and not inst:HasTag("player")
    then
        table.insert(actions, ACTIONS.ENEMYSELECT)
    end
end

AddComponentAction(type, component, testfn)

local state = "doshortaction"
AddStategraphActionHandler("wilson",ActionHandler(ACTIONS.ENEMYSELECT, state))
AddStategraphActionHandler("wilson_client",ActionHandler(ACTIONS.ENEMYSELECT,state))